// Section Our service Tabs
function showButtonInfo(evt, imgBlockId) {
    let imgBlock = document.getElementsByClassName('serv-content-all');
    for (let i = 0; i < imgBlock.length; i++) {
        imgBlock[i].style.display = 'none';
    }
    let btn = document.getElementsByClassName('service-button');
    for (let i = 0; i < btn.length; i++) {
        btn[i].className = btn[i].className.replace(' active', '');
    }
    document.getElementById(imgBlockId).style.display = 'flex';
    evt.currentTarget.className += ' active';
}


// Section Our amazing work START
const showImagesByCategory = (category) => {
    const allImages = Array.from(document.getElementsByClassName('work-img-box'));
    allImages.forEach(img => img.style.display = 'none');
    const hiddenImg = Array.from(document.getElementsByClassName('work-img-box2'));
    hiddenImg.forEach(img => img.style.display = 'none');
    switch (category) {
        case 'all' : {
            allImages.forEach(img => img.style.display = 'block');
            hiddenImg.forEach(img => img.style.display = 'block');
            break;
        }
        case 'gr_design' : {
            Array.from(document.getElementsByClassName('img-graphic-design')).forEach(img => img.style.display = 'block');
            break;
        }
        case 'web_design' : {
            Array.from(document.getElementsByClassName('img-web-design')).forEach(img => img.style.display = 'block');
            break;
        }
        case 'landing_page' : {
            Array.from(document.getElementsByClassName('img-landing-pages')).forEach(img => img.style.display = 'block');
            break;
        }
        case 'wordpress' : {
            Array.from(document.getElementsByClassName('img-wordpress')).forEach(img => img.style.display = 'block');
            break;
        }
    }
}

const loadMoreImages = (button) => {
    const btn = document.querySelector('.button-load-more');
    const hiddenImg = Array.from(document.getElementsByClassName('work-img-box2'));
    hiddenImg.forEach(img => img.style.display = 'none');
    if(button === 'load_more') {
        Array.from(document.getElementsByClassName('work-img-box2')).forEach(img => img.style.display = 'block');
        return btn.style.display = 'none';
    }
}
// Section Our amazing work END

//Section What People Say About theHam START
const aboutHeroWrapper = document.querySelectorAll('.about_hero-wrapper');
const sliderPhoto = document.querySelectorAll('.slider-photo');
const leftButton = document.querySelector('.left-btn');
const rightButton = document.querySelector('.right-btn');
leftButton.addEventListener('click', moveLeft);
function moveLeft(event) {
    let position = getActiveStatus();
    if(event.code === 'ArrowLeft' || event.type === 'click') {
        sliderPhoto.forEach((item, index, parent) => {
            if(position === 0) {
                parent[position].classList.remove('slider-active');
                parent[parent.length - 1].classList.add('slider-active');
                commentInit();
            } else {
                parent[position].classList.remove('slider-active');
                parent[position - 1].classList.add('slider-active');
                commentInit();
            }
        });
    }
}
rightButton.addEventListener('click', moveRight);
function moveRight(event) {
    let position = getActiveStatus();
    if(event.code === 'ArrowRight' || event.type === 'click') {
        sliderPhoto.forEach((item, index, parent) => {
            if(position === parent.length - 1) {
                parent[parent.length - 1].classList.remove('slider-active');
                parent[0].classList.add('slider-active');
                commentInit();
            } else {
                parent[position].classList.remove('slider-active');
                parent[position + 1].classList.add('slider-active');
                commentInit();
            }
        });
    }
}
document.addEventListener('keyup', moveLeft);
document.addEventListener('keyup', moveRight);

// get active slide element
function getActiveStatus() {
    for(let i = 0; i < sliderPhoto.length; i++) {
        if(sliderPhoto[i].classList.contains('slider-active')) {
            return i;
        }
    }
}

// Connection info block with slide
function commentInit() {
    let position = getActiveStatus();
    aboutHeroWrapper.forEach(item => {
        item.classList.remove('about_hero-wrapper-active');
        if(sliderPhoto[position].getAttribute('data-name') === item.id) {
            item.classList.add('about_hero-wrapper-active');
        }
    });
}

// Mouse click
sliderPhoto.forEach(item => {
    item.addEventListener('click', activeByMouse);
})
function activeByMouse() {
    sliderPhoto.forEach(item => {
        item.classList.remove('slider-active');
    });
    this.classList.add('slider-active');
    commentInit();
}
//Section What People Say About theHam END